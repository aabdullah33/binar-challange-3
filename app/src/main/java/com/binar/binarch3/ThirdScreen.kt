package com.binar.binarch3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.binar.binarch3.databinding.FragmentThirdScreenBinding

class ThirdScreen : Fragment() {

    private var _binding: FragmentThirdScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentThirdScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val aName = ThirdScreenArgs.fromBundle(arguments as Bundle).name
        binding.tvNama.text = "Namanya: $aName"

        binding.btnToScr4.setOnClickListener {
            findNavController().navigate(R.id.action_thirdScreen_to_fourthScreen)
        }

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Info>("inpo")
            ?.observe(viewLifecycleOwner) { result ->

                binding.tvUsia.visibility = View.VISIBLE
                binding.tvUsia.text = "Berusia ${result.usia} ${check(result.usia!!)}"
                binding.tvAlamat.visibility = View.VISIBLE
                binding.tvAlamat.text = "Tinggal di ${result.alamat}"
                binding.tvPekerjaan.visibility = View.VISIBLE
                binding.tvPekerjaan.text = "Bekerja sebagai ${result.pekerjaan}"
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    fun check(x: Int): String {
        if (x % 2 == 0) {
            return "(Genap)"
        } else {
            return "(Ganjil)"
        }
    }
}