package com.binar.binarch3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.binarch3.databinding.FragmentFourthScreenBinding
import com.binar.binarch3.databinding.FragmentSecondScreenBinding


class FourthScreen : Fragment() {

    private var _binding: FragmentFourthScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFourthScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnBackScr3.setOnClickListener { view ->
            if (binding.edtUsia.text.isNullOrEmpty() && binding.edtAlamat.text.isNullOrEmpty() &&
                binding.edtPekerjaan.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Isi semua data", Toast.LENGTH_SHORT)
                    .show()
            } else {
                val usia = (binding.edtUsia.text.toString()).toInt()
                val alamat = binding.edtAlamat.text.toString()
                val pekerjaan = binding.edtPekerjaan.text.toString()
                val info = Info(usia,alamat,pekerjaan)
                findNavController().previousBackStackEntry?.savedStateHandle?.set("inpo",info)
                findNavController().navigateUp()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}