package com.binar.binarch3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.binar.binarch3.databinding.FragmentSecondScreenBinding

class SecondScreen : Fragment() {

    private var _binding: FragmentSecondScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSecondScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnToScr3.setOnClickListener { view ->
            if (binding.edtNama.text.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "Isi semua data", Toast.LENGTH_SHORT)
                    .show()
            }else{
                val actionToFragmentKetiga =
                    SecondScreenDirections.actionSecondScreenToThirdScreen()
                actionToFragmentKetiga.name = binding.edtNama.text.toString()
                view.findNavController().navigate(actionToFragmentKetiga)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

}